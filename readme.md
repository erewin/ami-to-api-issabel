### Installation

1.add new user apiwatch to manager_custom.conf

[apiwatch]
secret = apiwatch
deny=0.0.0.0/0.0.0.0
permit=127.0.0.1/255.255.255.0
read = call

2. install by git php code

3. (optional) use supervisor for control launched script

4. set proper url and login details for asterisk manager in amidaemon.php
____________________
....amidamon.php....
...
$url = "http://157.230.17.235/cp/api";

$options = array(
    'host' => '127.0.0.1',
    'scheme' => 'tcp://',
    'port' => 5038,
    'username' => 'amiwatch',
    'secret' => 'amiwatch',
    'connect_timeout' => 10,
    'read_timeout' => 10
);
...
____________________


### Using

this system catch only calls from trunk with context started with "from-trunk" this is default context for all trunks in issabel
and also system will catch calls with context from-internal  that means call is made by extension.

main fields in API is callid and status
status:
 started means call is just started
 ringing means that call is ringing
 answered means that call is answered
 Ended means that call is ended (additional information you can get from cause,causetxt,status_explain)

your api reciever can ignore some request with some statuses if you need, like you can catch only started,answered,Ended

Examples of api:
outgoing call from exten:

GET /cp/api?callid=1592301074.21259&status=started&dialed=10447495228844&callerid=1003
GET /cp/api?callid=1592301074.21259&status=ringing
GET /cp/api?callid=1592301074.21259&status=busy
GET /cp/api?callid=1592301074.21259&cause=34&causetxt=Circuit/channelcongestion&status_explain=noanswered&status=Ended

incoming call from trunk noanswered by user:

GET /cp/api?callid=1592302965.21276&status=started&dialed=104474952284411&callerid=9227502213
GET /cp/api?callid=1592302965.21276&status=ringing
GET /cp/api?callid=1592302965.21276&cause=16&causetxt=NormalClearing&status_explain=noanswered&status=Ended

GET /cp/api?callid=1592302947.21274&status=started&dialed=104474952284411&callerid=9227502213
GET /cp/api?callid=1592302947.21274&status=ringing
GET /cp/api?callid=1592302947.21274&status=answered
GET /cp/api?callid=1592302947.21274&cause=16&causetxt=NormalClearing&status_explain=Normal


